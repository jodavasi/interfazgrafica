/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfazHerenciaVehiculo;

/**
 *
 * @author estudiante
 */
public class Furgoneta {
    public int capacidad;
    public boolean disponible;

    public Furgoneta(int capacidad, boolean disponible) {
        this.capacidad = capacidad;
        this.disponible = disponible;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }
    
    
}
